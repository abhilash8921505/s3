import express from "express";
import upload from "express-fileupload";
import dotenv from "dotenv";
import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";

import path from "path";
import { fileURLToPath } from "url";

dotenv.config();
const app = express();

// These are not available in ES6 module scope
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// S3 config
const client = new S3Client({
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  },
  region: process.env.S3_REGION,
});

// middleware
app.use(express.static("public"));
app.use(upload());

// health check
app.get("/", (_, res) => {
  res.send("Working!");
});

// file upload to server
app.post("/upload/server", (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send("No files were uploaded");
  }

  const file = req.files.file1;
  //   console.log(file);
  const uploadPath = __dirname + "/files/" + file.name.trim();

  file.mv(uploadPath, (err) => {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }
    res.send("File uploaded");
  });
});

// Upload to S3
app.post("/upload/s3", async (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send("No files were uploaded");
  }

  const file = req.files.file2;
  //   console.log(file);

  const url = await uploadFileToS3(file);
  return res.status(200).json({
    url,
  });
});

app.listen("3000", () => console.log("App running on http://localhost:3000"));

// helper functions
// function to upload file to S3
async function uploadFileToS3(file) {
  const bucketName = process.env.S3_BUCKET;
  const fileName = `sample/abhilash-${Date.now()}-${file.name.trim()}`;

  const params = {
    Bucket: bucketName,
    Key: fileName,
    Body: file.data,
    ContentType: file.mimetype,
    ContentDisposition: "inline",
    ACL: "public-read",
  };

  const uploadCommand = new PutObjectCommand(params);
  await client.send(uploadCommand);
  return `https://${bucketName}.s3.amazonaws.com/${fileName}`;
}
